package hu.xpathfactory.towerdefense.controller;


public class Clock extends Thread{
	private Controller controler;
	private int turnLength;
	private boolean isRunning;
	
	/**
	 * Clock konstruktora.
	 * @param turnLength mennyi idoegyseg teljen el ket ujkor kozott
	 * @param controler j�t�k controlerje
	 */
	public Clock (int turnLength, Controller controler) {
		this.turnLength = turnLength;
		this.controler = controler;
		this.isRunning = false;
	}

	@Override
	/**
	 * Szal futtatasaert felelos, meghivja a szamlalo fuggvenyt
	 */
	public void run() {
		isRunning = true;
		while(!this.controler.isItEnd() && isRunning){
			try {
				counting();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			controler.startNextTurn();
		}
	}
	
	/**
	 * Szamolja az eltelt idot
	 * @throws InterruptedException
	 */
	public void counting() throws InterruptedException{
		Thread.sleep(turnLength);
	}
	
	/**
	 * Megallitja az orat
	 */
	public void kill(){
		isRunning = false;
	}
}
