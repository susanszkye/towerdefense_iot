package hu.xpathfactory.towerdefense.controller;

import hu.xpathfactory.towerdefense.model.TowerTwoG;
import hu.xpathfactory.towerdefense.model.TowerThreeG;
import hu.xpathfactory.towerdefense.model.Cross;
import hu.xpathfactory.towerdefense.model.Map;
import hu.xpathfactory.towerdefense.model.TowerFiveG;
import hu.xpathfactory.towerdefense.model.Road;
import hu.xpathfactory.towerdefense.model.Saruman;
import hu.xpathfactory.towerdefense.model.Tower;
import hu.xpathfactory.towerdefense.model.TowerFourG;
import hu.xpathfactory.towerdefense.model.TowerOneG;
import hu.xpathfactory.towerdefense.util.Coordinate;
import hu.xpathfactory.towerdefense.view.GameView;
import hu.xpathfactory.towerdefense.view.ImageReader;
import hu.xpathfactory.towerdefense.view.MapView;
import hu.xpathfactory.towerdefense.view.SarumanView;
//import hu.xpathfactory.towerdefense.view.SarumanView;
import hu.xpathfactory.towerdefense.model.MobileStation;
import hu.xpathfactory.towerdefense.model.SimpleCall;
import hu.xpathfactory.towerdefense.model.Car;
import hu.xpathfactory.towerdefense.model.MobileNet;
import hu.xpathfactory.towerdefense.model.VideoStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class Controller {
	private static final int TURNS_TO_NEW_ENEMY = 5000; //500;
	public static int difficult;

	// 0: a kod nem jon, 1: a kod jon, 2: a kod random jon
	public static int FOG;

	// 0: nincs enemy kettevago lovedek, 1: csak az van, 2: random
	public static int SPLIT_ENEMY;

	// 1: jon random enemy, 0: nem jon
	public static int IS_RANDOM_ENEMY;

	// keresztezodesben a kovetkezo ut szama, -1: random
	public static int NEXT_ROAD;

	private static final int[] ROUNDS = {4,5,6,7,1, 0, 0, 0/*,17,20,25,30,35,40,45,50,60*/};
	
	private static final int TURNS_TO_NEW_WAVE = 5000;
	
	private static final int TURN_TO_DRAW = 50; //100;
	
	private Saruman saruman;
	private Map map;
	private Clock clock;
	public boolean usergame = true;
	private int turnsToNewEnemy = TURNS_TO_NEW_ENEMY;
	private GameView gameView;
	
	private int currentWave;
	private int currentEnemyInRound;
	private int turnsToNewWave;
	private int turnToDraw;
	private boolean lastWave;
	private int goodEnemyCount;
	private int nrOfMobileStations;
	private int maxMana;

	public Controller() {
		FOG = 2;
		SPLIT_ENEMY = 0;
		IS_RANDOM_ENEMY = 1;
		NEXT_ROAD = -1;
		gameView = new GameView(this);
		goodEnemyCount = 0;
		nrOfMobileStations = 0;
		maxMana = 3000;
	}

	public void setGoodEnemyCount() {
		goodEnemyCount++;
		if (goodEnemyCount == 5) {
			endGame(false);
		}
	}
	
	public int getGoodEnemyCount() {
		return goodEnemyCount;
	}
	
	/**
	 * �j j�t�k elind�t�sa
	 * 
	 * @param diff
	 *            a neh�zs�gi szint
	 */
	public void newGame(int diff) {
		if (diff == 1) {
			difficult = 0;
		}
		else {			
			difficult = 60;
		}
		this.saruman = new Saruman();
		this.clock = new Clock(1, this);
		this.map = new Map(this);
		
		SarumanView sarumanView = new SarumanView(saruman);
		MapView mapView = new MapView(map, new ImageReader());
		try {
			loadGame("testdefense_city_direction.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gameView.addMapView(mapView);
		gameView.addSarumanView(sarumanView);
		currentWave = 0;
		currentEnemyInRound = 1;
		turnsToNewWave = 0;
		turnToDraw = 0;
		lastWave = false;
		turnsToNewEnemy = 0;
	}

	/**
	 * Vege van e a jateknak
	 * 
	 * @return
	 */
	public boolean isItEnd() {
		return this.saruman.isItDead();
	}

	/**
	 * Beallitja hogy milyen modban van a jatek
	 * 
	 * @param b
	 *            ha igaz akkor jatekos jatszik, ha hamis akkor teszt fut
	 */
	public void setUserGame(boolean b) {
		this.usergame = b;
	}

	/**
	 * Elinditja a clockot
	 */
	public void startGame() {
		clock.start();
		map.gameStarted();
	}

	/**
	 * Megallitja a jatekot
	 */
	public void stopGame() {
		clock.kill();
	}

	public void loadGame(String mapPath) throws IOException {
		BufferedReader fbr = new BufferedReader(new FileReader("inputs\\" + mapPath));
		try {
			
			HashMap<Integer, ArrayList<Integer>> crossRoads = new HashMap<Integer, ArrayList<Integer>>();
			ArrayList<Integer> directions = new ArrayList<Integer>();
			String line;
			String state = "game";
			int roadnumber = -1;
			while ((line = fbr.readLine()) != null) {
				if (line.contains("map") && state == "game") {
					state = "map";
				} else if (line.contains("map") && state == "map") {
					state = "game";
				} else if ((line.contains("road")) && state == "map") {
					state = "road";
					roadnumber++;

					ArrayList<String> enemies = new ArrayList<String>();
					ArrayList<Integer> steps = new ArrayList<Integer>();

		//			ArrayList<Trap> traps = new ArrayList<Trap>();
					ArrayList<Coordinate> trapcoord = new ArrayList<Coordinate>();

				//	ArrayList<Rune> traprunes = new ArrayList<Rune>();
					ArrayList<Coordinate> runescoord = new ArrayList<Coordinate>();

					ArrayList<Coordinate> roadcoord = new ArrayList<Coordinate>();

					line = fbr.readLine();
					while (!line.contains("road")) {
						if (line.contains("point")) {
							
							line = fbr.readLine();
							int x = Integer.parseInt(line.substring(
									line.indexOf('>') + 1,
									line.indexOf('/') - 1));
							line = fbr.readLine();
							int y = Integer.parseInt(line.substring(
									line.indexOf('>') + 1,
									line.indexOf('/') - 1));
							Coordinate cr = new Coordinate(x, y);
							// cr.setX(x);
							// cr.setY(y);
							roadcoord.add(cr);
							line = fbr.readLine();
						} else if (line.contains("enemy")) {
							line = fbr.readLine();
							String race = line.substring(line.indexOf('>') + 1,
									line.indexOf('/') - 1);
							line = fbr.readLine();
							int step = Integer.parseInt(line.substring(
									line.indexOf('>') + 1,
									line.indexOf('/') - 1));
							enemies.add(race);
							steps.add(step);
							line = fbr.readLine();
						} else if (line.contains("trap")) {
							line = fbr.readLine();
							//Trap trap = new Trap();
							//traps.add(trap);
							Coordinate trc = new Coordinate();
							if (line.contains("koordinate")) {
								line = fbr.readLine();
								trc.setX(Integer.parseInt(line.substring(
										line.indexOf('>') + 1,
										line.indexOf('/') - 1)));
								line = fbr.readLine();
								trc.setY(Integer.parseInt(line.substring(
										line.indexOf('>') + 1,
										line.indexOf('/') - 1)));
								line = fbr.readLine();
								trapcoord.add(trc);
							}

							line = fbr.readLine();
//							if (line.contains("rune")) {
//								String rune = line.substring(
//										line.indexOf('>') + 1,
//										line.indexOf('/') - 1);
//								if (rune.equals("BlackRune")) {
//									traprunes.add(pickNewRune(1));
//									runescoord.add(trc);
//								}
//								if (rune.equals("BlueRune")) {
//									traprunes.add(pickNewRune(2));
//									runescoord.add(trc);
//								}
//								if (rune.equals("RedRune")) {
//									traprunes.add(pickNewRune(3));
//									runescoord.add(trc);
//								}
//								if (rune.equals("YellowRune")) {
//									traprunes.add(pickNewRune(4));
//									runescoord.add(trc);
//								}
//								if (rune.equals("GreenRune")) {
//									traprunes.add(pickNewRune(5));
//									runescoord.add(trc);
//								}
//								line = fbr.readLine();
//							}

						} else if (line.contains("cross")) {
							ArrayList<Integer> road = new ArrayList<Integer>();
							while ((line = fbr.readLine()).contains("next_road")) {
								road.add(Integer.parseInt(line.substring(
										line.indexOf('>') + 1,
										line.indexOf('/') - 1)));
							}
							crossRoads.put(roadnumber, road);

						} else if (line.contains("direction")) {
							int i = (Integer.parseInt(line.substring(line.indexOf('>') + 1,	line.indexOf('/') - 1)));
							directions.add(i);
						}
						line = fbr.readLine();
					}
					addRoadToMap(roadcoord);
					for (int i = 0; i < enemies.size(); i++) {
						if (enemies.get(i).equals("human")) {
							createEnemy(1, roadnumber, steps.get(i));

						}
						if (enemies.get(i).equals("hobbit")) {
							createEnemy(2, roadnumber, steps.get(i));
						}
						if (enemies.get(i).equals("dwarf")) {
							createEnemy(3, roadnumber, steps.get(i));
						}
						if (enemies.get(i).equals("elf")) {
							createEnemy(4, roadnumber, steps.get(i));
						}
					}
//					for (int i = 0; i < traps.size(); i++) {
//						Trap(trapcoord.get(i), traps.get(i));
//					}
			/*		for (int i = 0; i < traprunes.size(); i++) {
						placeRune(runescoord.get(i), traprunes.get(i));
					} */
					state = "map";

				}

				else if ((line.contains("tower")) && state == "map") {
					line = fbr.readLine();
					Coordinate tc = new Coordinate();
					Tower t = null;

					if (line.contains("koordinate")) {
						line = fbr.readLine();
						tc.setX(Integer.parseInt(line.substring(
								line.indexOf('>') + 1, line.indexOf('/') - 1)));
						line = fbr.readLine();
						tc.setY(Integer.parseInt(line.substring(
								line.indexOf('>') + 1, line.indexOf('/') - 1)));
						line = fbr.readLine();
					}

					line = fbr.readLine();
					String type = line.substring(line.indexOf('>') + 1,
							line.indexOf('/') - 1);
					if (type.equals("black")) {
						t = pickNewTower(1);
					}
					if (type.equals("blue")) {
						t = pickNewTower(2);
					}
					if (type.equals("red")) {
						t = pickNewTower(3);
					}
					placeTower(tc, t);
						
					line = fbr.readLine();
					if (line.contains("rune")) {
						/*String rune = line.substring(line.indexOf('>') + 1,
								line.indexOf('/') - 1);
						if (rune.equals("BlackRune")) {
							placeRune(tc, pickNewRune(1));
						}
						if (rune.equals("BlueRune")) {
							placeRune(tc, pickNewRune(2));
						}
						if (rune.equals("RedRune")) {
							placeRune(tc, pickNewRune(3));
						}
						if (rune.equals("YellowRune")) {
							placeRune(tc, pickNewRune(4));
						}
						if (rune.equals("GreenRune")) {
							placeRune(tc, pickNewRune(5));
						} */
					}
					line = fbr.readLine();
				}

				else if (line.contains("saruman")) {
					line = fbr.readLine();
					setManna(Integer.parseInt(line.substring(
							line.indexOf('>') + 1, line.indexOf('/') - 1)));
					line = fbr.readLine();
				}

			}
			Set<Integer> cross = crossRoads.keySet();
			for (Integer i : cross) {
				ArrayList<Road> roads = new ArrayList<Road>();
				for (Integer j : crossRoads.get(i)) {
					roads.add(getMap().getRoads().get(j));
				}
				Cross c = new Cross(250, roads);
				getMap().getRoads().get(i).setCross(c);
			}
			int i = 0;
			for(Road r: getMap().getRoads()) {
				r.setDirection(directions.get(i));
				i++;
			}
			// gameState = GameState.loaded;
		} catch (IllegalArgumentException e) {
			System.out.println(e);
		} finally {
			fbr.close();
		}
	}

	/**
	 * Ellenf�l hal�la miatt megn�veli szarum�n man�j�t
	 */
	public void enemyDied() {
		saruman.addMana(MobileStation.PRICE);
	}

	/**
	 * V�ge a j�t�knak
	 * 
	 * @param b
	 */
	public void endGame(boolean b) {
		if(b) {
			gameView.setSarumanLabelText(":(  :(  :(");
		} else {
			gameView.setSarumanLabelText("(: WIN (:");
		}
		if(saruman.getMana() > maxMana) {
			maxMana = saruman.getMana();
		}
		if(saruman.getMana() > 9000) {
			maxMana = 9000;
		}
		this.saruman.setDeath(b);
		gameView.draw();
		this.clock.kill();
	}

	/**
	 * Jelzi a mapnek, hogy l�ptese az ellenfeleket, illetve automatikkusan
	 * letrehoz ellenfeleket, elore definialt idokozonkent
	 */
	public void startNextTurn() {
		if(saruman.getMana() < 0) {
			this.endGame(true);
		}
		turnsToNewEnemy--;
		if(turnsToNewWave <= 0 && !lastWave) {
			if(turnsToNewEnemy <= 0){
				if(currentEnemyInRound <= ROUNDS[currentWave]) {
					if(Controller.IS_RANDOM_ENEMY == 1) {
						Random randomGenerator = new Random();
						int roadNumber = randomGenerator.nextInt(4);
						int r = 0;
						if (currentWave < 2) {
							r = randomGenerator.nextInt(2);
//							enemyLevel = 3;
//						} else if (currentWave > 1) {
//							enemyLevel = 4;
						} else if(currentWave > 1) {
							r = randomGenerator.nextInt(2) + 2;
						}
						createEnemy(r+1, roadNumber);
						currentEnemyInRound++;
					}
				} else {
					currentEnemyInRound = 1;
					currentWave++;
					turnsToNewWave = TURNS_TO_NEW_WAVE;
					if(currentWave == ROUNDS.length){
						this.lastWave = true;
					}
				}
				turnsToNewEnemy = TURNS_TO_NEW_ENEMY - 20*currentWave;
			}
		} else {
			turnsToNewWave--;
		}
		map.nextTurnStarted();
		
		if(turnToDraw<=0) {
			gameView.draw();
			
			turnToDraw = TURN_TO_DRAW;
		} else {
			turnToDraw--;
		}
		if (lastWave){
			endGame(false);
		}
	}

	/**
	 * A kapott k�vet leteszi a megadott helyre
	 * 
	 * @param c
	 *            A koordin�ta
	 * @param r
	 *            A k�
	 * @return true ha a lerakas sikeres
	 */

	/**
	 * �j k� kiv�laszt�sa.
	 * 
	 * @throws IOException
	 */
//	public Rune pickNewRune(int runeNumber) {
//		int cost = Rune.PRICE;
//		if (saruman.hasEnoughMana(cost)) {
//			switch (runeNumber) {
//			case 1:
//				return new BlackRune();
//			case 2:
//				return new BlueRune();
//			case 3:
//				return new RedRune();
//			case 4:
//				return new YellowRune();
//			case 5:
//				return new GreenRune();
//			}
//		}
//		return null;
//	}

	/**
	 * Ha lehet, a tornyot hozz�adja a map-hez.
	 * 
	 * @param c
	 *            koordin�ta, ahova tenni szeretn�
	 * @param t
	 *            torony, amit le akar tenni
	 * @return true ha a lerakas sikeres
	 */
	public boolean placeTower(Coordinate c, Tower t) {
		if (!map.isThereAnything(c, 30)) {
			map.addTower(t);
			t.setPosition(c);
			saruman.addMana(-t.getPrice());
			return true;
		}
		return false;
	}

	/**
	 * Visszad egy tornyot, ha van el�g man�ja Szarum�nnak.
	 * 
	 * @throws IOException
	 */
	public Tower pickNewTower(int towerNumber) {		
		switch (towerNumber) {
		case 1:
			if (saruman.hasEnoughMana(TowerOneG.PRICE)) {
				return new TowerOneG(map);
			}
		case 2:
			if (saruman.hasEnoughMana(TowerTwoG.PRICE)) {
				return new TowerTwoG(map);
			}
		case 3:
			if (saruman.hasEnoughMana(TowerThreeG.PRICE)) {
				return new TowerThreeG(map);
			}
		case 4:
			if (saruman.hasEnoughMana(TowerFourG.PRICE)) {
				return new TowerFourG(map);
			}
		case 5:
			if (saruman.hasEnoughMana(TowerFiveG.PRICE)) {
				return new TowerFiveG(map);
			}
		}
		return null;
	}


	/**
	 * Elk�sz�t egy ellenfelet �s azt hozz�adja a kiv�lasztott �thoz
	 * 
	 * @return igaz ha sikeresen letrejott az ellenfel
	 */
	public boolean createEnemy(int r, int roadNumber) {
		MobileStation e = null;
		switch (r) {
		case 1:
			e = new SimpleCall();
			break;
		case 2:
			e = new MobileNet();
			break;
		case 3:
			e = new VideoStream();
			break;
		case 4:
			e = new Car();
			break;
		}
		map.addEnemyToRoad(roadNumber, e);
		if (e == null)
			return false;
		nrOfMobileStations++;
		return true;
	}

	/**
	 * Elk�sz�t egy ellenfelet �s azt hozz�adja a kiv�lasztott �thoz
	 * 
	 * @return igaz ha sikeresen letrejott az ellenfel
	 */
	public boolean createEnemy(int r, int roadNumber, int steps) {
		MobileStation e = null;
		switch (r) {
		case 1:
			e = new SimpleCall();
			break;
		case 2:
			e = new MobileNet();
			break;
		case 3:
			e = new VideoStream();
			break;
		case 4:
			e = new Car();
			break;
		}
		if (e == null)
			return false;
		map.addEnemyToRoad(roadNumber, e);
		e.setStepsCount(steps);
		return true;
	}

	/**
	 * Lekeri az adott koordinataju torony sebzeset
	 * 
	 * @param c
	 *            A torony koordinatai
	 * @return Az koordinataban megadott torony sebzese
	 */
	public int getTowerDamageFromMap(Coordinate c) {
		return map.getTowerFireRate(c);
	}

	/**
	 * Lekeri az adott koordinataju torony tuzelesi sebesseget
	 * 
	 * @param c
	 *            A torony koordinatai
	 * @return Az koordinataban megadott torony tuzelesi sebessege
	 */
	public int getTowerFireRangeFromMap(Coordinate c) {
		return map.getTowerFireRate(c);
	}

	/**
	 * Adott koordinata listaju utat hozzadja a terkephez
	 * 
	 * @param cords
	 *            A koordinata lista
	 */
	public void addRoadToMap(ArrayList<Coordinate> cords) {
		map.addRoad(cords);
	}

	/**
	 * Visszaadja a map-et
	 * 
	 * @return A map
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * Visszaadja Sarumant
	 * 
	 * @return Saruman
	 */
	public Saruman getSaruman() {
		return saruman;
	}

	/**
	 * Beallitja Saruman manajat
	 * 
	 * @param m
	 *            A mana erteke
	 */
	public void setManna(int m) {
		saruman.setMana(m);
	}

	public void setGoodEnemyCount(int i) {
		goodEnemyCount = 0;
	}

	public void addSarumanMana(int mana) {
		saruman.addMana(mana);
	}

	public int maxMana() {
		// TODO Auto-generated method stub
		return maxMana;
	}

}
