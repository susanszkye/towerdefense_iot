package hu.xpathfactory.towerdefense.controller;

import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import hu.xpathfactory.towerdefense.util.GameState;

public class Main {

	private static GameState gameState;
	private static Controller controller;

	/**
	 * @param args
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public static void main(String[] args) throws IOException,
			XMLStreamException {
		controller = new Controller();
		gameState = GameState.menu;
	}
}
