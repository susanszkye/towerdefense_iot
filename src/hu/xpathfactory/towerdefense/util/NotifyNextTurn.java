package hu.xpathfactory.towerdefense.util;
import java.io.IOException;


public interface NotifyNextTurn {
	
	/**
	 * �j k�r ind�t�sa.
	 * @return igaz, ha a l�p�s v�grehajt�dott, hamis, ha nem
	 */
	boolean nextTurnStarted() throws NumberFormatException, IOException;
}
