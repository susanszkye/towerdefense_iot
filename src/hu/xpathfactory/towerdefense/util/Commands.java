package hu.xpathfactory.towerdefense.util;

public enum Commands {
	newGame, start, backToMenu, getStaff, exit, dumpRedTower, dumpBlackTower, dumpBlueTower,
	dumpTrap, putBlackRune, putBlueRune, putYellowRune, putRedRune, putGreenRune, setRandomFogDown,
	fogDown, getTowerFireAttribute, setRandomRoadSelector, setRoadToCross, setRandomTowerAmmo,
	setTowerAmmo, runTestCase, loadGame, nextTurn, showGame, createEnemy, setRandomness
}
