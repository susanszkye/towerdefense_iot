package hu.xpathfactory.towerdefense.util;

public class Parser {

	public static final int windowWidth = 800;
	public static final int windowHeight = 800;

	/**
	 * A kapott stringr�l eld�nti, hogy sz�m-e
	 * 
	 * @param s
	 * @return
	 */
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * Megallapitja hogy a parameterben kapott koordinata koordinata-e es ha igen
	 * akkor a terkepen rajta van e az adott pont
	 * @param x
	 * 			x koordinata
	 * @param y
	 * 			y koordinata
	 * @return
	 * 			igaz ha a koordinata valid, hamis ha nem
	 */
	public static boolean isValidCoordinate(String x, String y) {
		if (isInteger(x) && isInteger(y)) {
			if (Integer.parseInt(x) >= 0 && Integer.parseInt(x) <= windowWidth
					&& Integer.parseInt(y) >= 0
					&& Integer.parseInt(y) <= windowHeight) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Paramterul kapott stringekrol eldonti hogy koordinatak e es ha igen akkor visszaadja
	 * koordinata tipuskent
	 * @param x
	 * 			x koordinata
	 * @param y
	 * 			y koordinata
	 * @return
	 * 			koordinatava konvertalt koordinata
	 */
	public static Coordinate stringToCoordinate(String x, String y){
		if(isValidCoordinate(x, y)){
			int cx = Integer.parseInt(x);
			int cy = Integer.parseInt(y);
			return new Coordinate(cx, cy);
		}
		return null;
	}
}
