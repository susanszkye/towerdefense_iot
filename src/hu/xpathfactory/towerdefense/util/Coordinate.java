package hu.xpathfactory.towerdefense.util;

public class Coordinate {
	private int x;
	private int y;
	
	/**
	 * Default konstruktor. 0-ra �ll�tja a koordin�t�akat.
	 */
	public Coordinate(){
		x = 0;
		y = 0;
	}
	
	/**
	 * Koordin�ta konstruktor
	 * @param x x koordin�ta
	 * @param y y koordin�ta
	 */
	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return (x + " " + y);
	}
	
	/**
	 * Beallitja az x koordinatat
	 * @param x
	 * 			A beallitani kivant ertek
	 */
	public void setX(int x){
		this.x = x;
	}
	
	/**
	 * Beallitja az y koordinatat
	 * @param y
	 * 			A beallitani kivant ertek
	 */
	public void setY(int y){
		this.y = y;
	}
	
	/**
	 * Lekeri az x koordinatat
	 * @return
	 * 			Az x koordinata
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Lekeri az y koordinatat
	 * @return
	 * 			Az y koordinata
	 */
	public int getY(){
		return y;
	}
}
