package hu.xpathfactory.towerdefense.view;

import hu.xpathfactory.towerdefense.model.MobileStation;
import hu.xpathfactory.towerdefense.model.Road;
import hu.xpathfactory.towerdefense.util.Coordinate;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class RoadView {
	private Road road;
	private ArrayList<EnemyView> enemyviews;
	private static BufferedImage pancake;
	
	public RoadView(Road road, ImageReader imageReader){
		pancake = imageReader.readImage("textures/road.png");
		this.road = road;
		
		enemyviews = new ArrayList<EnemyView>();
		for (int i = 0; i < road.getEnemies().size(); i++ ) {
			enemyviews.add(new EnemyView(road.getEnemies().get(i)));
		}
	}
	
	public void draw(Graphics g) {	
	/*	if(road.getDirection() % 90 == 0) {
			for(int i = 0; i < road.getRoadLenght(); i+=60){
				Coordinate coordinate = road.getCoordinate(i);
				int topLeftX = coordinate.getX() - pancake.getWidth() / 2;
				int topLeftY = coordinate.getY() - pancake.getHeight() / 2;
				g.drawImage(pancake, topLeftX, topLeftY, null);
			}
		} else {
			for(int i = 0; i < road.getRoadLenght(); i+=40){
				Coordinate coordinate = road.getCoordinate(i);
				int topLeftX = coordinate.getX() - pancake.getWidth() / 2;
				int topLeftY = coordinate.getY() - pancake.getHeight() / 2;
				g.drawImage(pancake, topLeftX, topLeftY, null);
			}
		}*/
		
	}
	
		
		
	public void drawTrapsAndEnemies(Graphics g){
		ArrayList<EnemyView> enemyViewsToRemove = new ArrayList<EnemyView>();
		for (EnemyView enemyView : enemyviews) {
			MobileStation enemy = enemyView.getEnemy();
			if (!this.equals(enemy.getRoad()) || !road.getEnemies().contains(enemy)) {
				enemyViewsToRemove.add(enemyView);
			}
		}
		
		enemyviews.removeAll(enemyViewsToRemove);
		
		ArrayList<MobileStation> enemiesOnRoad = new ArrayList<MobileStation>(road.getEnemies());
		for (EnemyView enemyView : enemyviews) {
			MobileStation enemy = enemyView.getEnemy();
			enemiesOnRoad.remove(enemy);
		}
		for (MobileStation enemy : enemiesOnRoad) {
			enemyviews.add(new EnemyView(enemy));
		}

		for (EnemyView ew: enemyviews) {
			ew.draw(road.getPoints().get(ew.getEnemy().getNumberOfSteps()),g);
		}
	}
		
		
		
		
		
//		public void drawTrapsAndEnemies(Graphics g){
//		if(road.getEnemies().size() == 0 && enemyviews.size() != 0){
//			enemyviews.remove(0);
//		}
//		
//		for (int i = 0; i < road.getEnemies().size(); i++ ) {
//			if(i < enemyviews.size()){
//				if(!enemyviews.get(i).getEnemy().equals(road.getEnemies().get(i))){
//					enemyviews.remove(i);
//					i--;
//				}
//			} else {				
//				enemyviews.add(new EnemyView(road.getEnemies().get(i)));
//			}
//		}
//		
//		for (EnemyView ew: enemyviews) {
//			ew.draw(road.getPoints().get(ew.getEnemy().getNumberOfSteps()),g);
//		}
//	}
}
