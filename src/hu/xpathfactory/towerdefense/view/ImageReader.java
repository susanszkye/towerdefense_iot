package hu.xpathfactory.towerdefense.view;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class ImageReader {
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	double ratio;
	private static final double X_ORIG_SIZE = 1920;
	private static final double Y_ORIG_SIZE = 1080;
	
	public ImageReader() {
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		
		double x_ratio = width/X_ORIG_SIZE;
		double y_ratio = height/Y_ORIG_SIZE;
		
		if(x_ratio > y_ratio) {
			ratio = y_ratio;
		} else {
			ratio = x_ratio;
		}
	}
	
	public BufferedImage readImage(String path) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(path));
//			int origWidth = origSizeImage.getWidth();
//			int origHeight = origSizeImage.getHeight();
			
//			Image thumbnail = origSizeImage.getScaledInstance((int)(origWidth * ratio), (int)(origHeight * ratio), Image.SCALE_SMOOTH);
//			img = new BufferedImage(thumbnail.getWidth(null), thumbnail.getHeight(null), BufferedImage.TYPE_INT_ARGB);

			return img;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
