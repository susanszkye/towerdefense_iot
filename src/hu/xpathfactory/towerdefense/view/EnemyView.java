package hu.xpathfactory.towerdefense.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import hu.xpathfactory.towerdefense.model.MobileStation;
import hu.xpathfactory.towerdefense.model.EndUserType;
import hu.xpathfactory.towerdefense.util.Coordinate;

public class EnemyView {
	private MobileStation enemy;
	private BufferedImage img = null; 
	private static final BasicStroke stroke = new BasicStroke(4);
	
	public EnemyView(MobileStation enemy){
		this.enemy = enemy;
		EndUserType race = enemy.getRace();
		//	String filePath = "textures" + File.separator + race.toString() + ".png";
			String filePath = "textures" + File.separator + race.toString() + ".png";
		try {
			img = ImageIO.read(new File(filePath));
		} catch (IOException e) { }
	}
	
	
	public void draw(Coordinate c, Graphics g){
////////////////////////////

		BufferedImage bf;
		if(enemy.getRace().toString() == "Car") {
			double rotation = enemy.roadDirection();
			if (rotation == 90) {
				AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
				tx.translate(-img.getWidth(null), 0);
				AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
				bf = op.filter(img, null);
			} else {
				double rotationRequired = Math.toRadians (enemy.roadDirection() + 90);
				double locationX = img.getWidth() / 2;
				double locationY = img.getHeight() / 2;
				AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
				AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
				bf = op.filter(img, null);
			}
		} else {
			bf = img;
		}
		//float ratio = (float) enemy.getDataToHappiness() / (float) enemy.getData();
		int happiness = enemy.getHappinessLevel();
		g.drawImage(bf, c.getX() - img.getWidth() / 2, c.getY() - img.getHeight() / 2,null);
		
		int circleSize = 40;
		
		g.setColor(Color.GRAY);
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(stroke);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawArc(c.getX() - circleSize/2, c.getY() - circleSize/2, circleSize, circleSize, 0, 360);

		if(happiness < 0) {
			g.setColor(Color.RED);
		} else {
			g.setColor(Color.GREEN);
		}
		//g.drawString(Integer.toString(enemy.getHappinessLevel()), c.getX(), c.getY());
		g.drawArc(c.getX() - circleSize/2, c.getY() - circleSize/2, circleSize, circleSize, 90, -36*happiness);
	}

//	//public void draw(Graphics g) {	
//		int topLeftX = position.getX() - programmer.getWidth() / 2;
//		int topLeftY = position.getY() - programmer.getHeight() / 2;
//		g.drawImage(programmer, topLeftX, topLeftY, null);
//		
//		////////////////////////////
//		
//	//	if(enemy.getData() != 0) {
//			int width = (int)((float)img.getWidth() / ratio);
//			int height = (int)((float)img.getHeight() / ratio);
//			/*ratio = (float) enemy.getDataToHappiness() / (float) enemy.getData();
//			width = (int)((float)img.getWidth() / ratio);
//			height = (int)((float)img.getHeight() / ratio);*/
//			AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
//			AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
//			img = op.filter(image, null);
//	//	}
//	}


	public MobileStation getEnemy() {
		return enemy;
	}
}
