package hu.xpathfactory.towerdefense.view;


import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


import hu.xpathfactory.towerdefense.util.Coordinate;

public class ProgrammerView {
	
	private static BufferedImage image;
	private BufferedImage programmer;
	private Coordinate position;
	
	public ProgrammerView(Coordinate c, int direction, ImageReader imageReader) {
		image = imageReader.readImage("textures/programmer_tomap0.png");
		
		position = c;

		double rotationRequired = Math.toRadians (direction);
		double locationX = image.getWidth() / 2;
		double locationY = image.getHeight() / 2;
		AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
		programmer= op.filter(image, null);
	}

	public void draw(Graphics g) {	
//		int topLeftX = position.getX() - programmer.getWidth() / 2;
//		int topLeftY = position.getY() - programmer.getHeight() / 2;
//		g.drawImage(programmer, topLeftX, topLeftY, null);
	}
}