package hu.xpathfactory.towerdefense.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import hu.xpathfactory.towerdefense.model.Tower;
import hu.xpathfactory.towerdefense.util.Coordinate;

public class TowerView {
	private Tower tower;
	private BufferedImage img;
	private static final BasicStroke stroke = new BasicStroke(2);
	
	private Coordinate c = null;
	private int count = 0;
	private int pictureNr = 0;
	
	public TowerView(Tower tower, ImageReader imageReader) {
		this.tower = tower;
		String generation = tower.getGeneration();

		img = imageReader.readImage("textures" + File.separator + "torony" + generation + "G.png");
	}
	
	public void draw(Graphics g, int money) {
		
		Coordinate position = tower.getCoordinate();
		
		int towerTopLeftX = tower.getX() - img.getWidth() / 2;
		int towerTopLeftY = tower.getY() - img.getHeight() + 10;
		g.drawImage(img, towerTopLeftX, towerTopLeftY, null);
		
		if(tower.isShomethingToShoot() && count == 0){
			c = tower.whereToShoot();			
			count = 1;
		}
		
		if(count != 0) {
			g.setColor(java.awt.Color.RED);

			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke(stroke);
			g.drawLine(tower.getX(), tower.getY()-25, c.getX(), c.getY());
			count--;
		}
		
		int range;

		switch(tower.getColor()) {
		case Green: g.setColor(new Color(255, 0, 220, 40)); break;
		case Black: g.setColor(new Color(182, 255, 0, 40)); break;
		case Blue: g.setColor(new Color(255, 216, 0, 40)); break;
		case Yellow: g.setColor(new Color(178, 0, 255, 40)); break;
		case Red: g.setColor(new Color(0, 38, 255, 40)); break;
		default:
			break;
		}
		range = tower.getFireRange();
		g.fillOval(position.getX()-range, position.getY()-range, range*2, range*2);
		
		
		if(pictureNr < 2) {
			pictureNr++;
		} else {
			pictureNr = 0;
		}
	}
}
