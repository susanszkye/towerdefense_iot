package hu.xpathfactory.towerdefense.view;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import hu.xpathfactory.towerdefense.controller.Controller;
import hu.xpathfactory.towerdefense.model.Tower;
import hu.xpathfactory.towerdefense.util.Coordinate;

public class GameView implements ActionListener {

	private MapView mapView;
    private SarumanView sarumanView;
	private MapPanel mapPanel;
	private JPanel controllPanel;
	private Controller controller;
	private JFrame gameFrame;
	private JPanel menuPanel;
	private JLabel sarumanLabel;
	private JPanel sarumanPanel;
	private JButton selectedButton;
	private ArrayList<JButton> buttonList;
	BufferedImage towerPriceShow;
	

	
	public GameView(Controller controller)
	{
		this.controller = controller;
		gameFrame = new JFrame();
		init();
	}
	
	private class MapPanel extends JPanel{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			super.paintComponent(g);
			mapView.draw(g);
		}
	}
	
	public class DrawRectPanel extends JPanel  {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			super.paintComponent(g);
//			g.setColor(Color.LIGHT_GRAY);
//			g.fillRoundRect(0, 50, 900, 30, 10, 10);
			g.drawImage(towerPriceShow, 0, 0, null);
			g.setColor(Color.RED);
			g.fillRoundRect(0, 50, controller.maxMana()/10, 30, 10, 10);
			g.setColor(Color.GREEN);
			g.fillRoundRect(0, 50, controller.getSaruman().getMana()/10, 30, 10, 10);
		}
	 }
	
	public void addMapView(MapView mapView)
	{
		this.mapView = mapView;
	}
	
	public void addSarumanView(SarumanView sarumanVw)
	{
		this.sarumanView = sarumanVw;
		sarumanView.addGameView(this);
	}
	
	public void draw()
	{
		mapPanel.repaint();
		sarumanPanel.repaint();
	}
	
	private void init()
	{
		buttonList = new ArrayList<JButton>();
		
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.setTitle("TEST DEFENSE");
		gameFrame.setResizable(true);
		gameFrame.setSize(1920, 1080);
		//gameFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		gameFrame.setUndecorated(true);
		gameFrame.setLayout(new BorderLayout());
		
		menuPanel = new JPanel();
		menuPanel.setPreferredSize(new Dimension(1880, 1050));
		menuPanel.setBackground(Color.darkGray);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(200, 500));
		buttonPanel.setBackground(Color.BLACK);
		
		JButton newGameAdvanceBtn = new JButton("START");
		newGameAdvanceBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				controller.newGame(2);
				setNewGame();
			}
		});
		
		newGameAdvanceBtn.setPreferredSize(new Dimension(100, 40));
		buttonPanel.add(newGameAdvanceBtn);
		menuPanel.add(buttonPanel);
	    gameFrame.add(menuPanel, BorderLayout.CENTER);
	    gameFrame.setVisible(true);
	}
	
	private void setNewGame()
	{		
		mapPanel = new MapPanel();
	    mapPanel.setPreferredSize(new Dimension(1700, 900));
	    mapPanel.addMouseListener(new MouseAdapter() {
	    	@Override
			public void mouseClicked(MouseEvent e) {
				if (selectedButton != null) {
					Coordinate coordinate = new Coordinate(e.getX(), e.getY());

					String label = selectedButton.getName();
					Tower tower = null;

					if (label.equals("greenTower")) {
						tower = controller.pickNewTower(1);
					} else if (label.equals("blackTower")) {
						tower = controller.pickNewTower(2);
					} else if (label.equals("blueTower")) {
						tower = controller.pickNewTower(3);
					} else if (label.equals("yellowTower")) {
						tower = controller.pickNewTower(4);
					} else if (label.equals("redTower")) {
						tower = controller.pickNewTower(5);
					}

					if (tower != null) {
						if(controller.placeTower(coordinate, tower)) {
							selectedButton.setOpaque(false);
							selectedButton.setContentAreaFilled(false);
							selectedButton = null;
						}
					}
					
					mapPanel.repaint();
				}

			}
		});
	    
	    controllPanel = new JPanel(new FlowLayout());
	    controllPanel.setBorder(null);
	    controllPanel.setPreferredSize(new Dimension(1920, 180));
	    controllPanel.setOpaque(false);

	    ImageIcon newGame = new ImageIcon("textures" + File.separator + "newgame.png");
	    JButton menuBtn = new JButton(newGame);
	    menuBtn.setBorderPainted(false);
	    menuBtn.setPreferredSize(new Dimension(130, 130));
	    menuBtn.setOpaque(false);
	    menuBtn.setContentAreaFilled(false);
	    menuBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				controller.endGame(true);
				gameFrame.remove(mapPanel);
				gameFrame.remove(controllPanel);
				gameFrame.repaint();
				controller.setGoodEnemyCount(0);
				
				controller.newGame(2);
				setNewGame();
			}
		});
	    
	    try {
			towerPriceShow = ImageIO.read(new File("textures" + File.separator + "towerpriceshow.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    ImageIcon blueTower = new ImageIcon("textures" + File.separator + "torony3G_button.png");
	    JButton blueTowerBtn = new JButton(blueTower);
	    blueTowerBtn.setBorderPainted(false);
	    blueTowerBtn.setName("blueTower");
	    blueTowerBtn.setFocusable(false);
	    blueTowerBtn.setOpaque(false);
	    blueTowerBtn.setContentAreaFilled(false);
	    blueTowerBtn.addActionListener(this);
	    blueTowerBtn.setPreferredSize(new Dimension(100, 120));
	    buttonList.add(blueTowerBtn);

	    ImageIcon blackTower = new ImageIcon("textures" + File.separator + "torony2G_button.png");
	    JButton blackTowerBtn = new JButton(blackTower);
	    blackTowerBtn.setBorderPainted(false);
	    blackTowerBtn.setName("blackTower");
	    blackTowerBtn.setFocusable(false);
	    blackTowerBtn.setOpaque(false);
	    blackTowerBtn.setContentAreaFilled(false);
	    blackTowerBtn.addActionListener(this);
	    blackTowerBtn.setPreferredSize(new Dimension(100, 120));
	    buttonList.add(blackTowerBtn);

	    ImageIcon redTower = new ImageIcon("textures" + File.separator + "torony5G_button.png");
	    JButton redTowerBtn = new JButton(redTower);
	    redTowerBtn.setBorderPainted(false);
	    redTowerBtn.setName("redTower");
	    redTowerBtn.setFocusable(false);
	    redTowerBtn.setOpaque(false);
	    redTowerBtn.setContentAreaFilled(false);
	    redTowerBtn.addActionListener(this);
	    redTowerBtn.setPreferredSize(new Dimension(100, 120));
	    buttonList.add(redTowerBtn);

	    ImageIcon yellowTower = new ImageIcon("textures" + File.separator + "torony4G_button.png");
	    JButton yellowTowerBtn = new JButton(yellowTower);
	    yellowTowerBtn.setBorderPainted(false);
	    yellowTowerBtn.setName("yellowTower");
	    yellowTowerBtn.setFocusable(false);
	    yellowTowerBtn.setOpaque(false);
	    yellowTowerBtn.setContentAreaFilled(false);
	    yellowTowerBtn.addActionListener(this);
	    yellowTowerBtn.setPreferredSize(new Dimension(100, 120));
	    buttonList.add(yellowTowerBtn);
	    
	    ImageIcon greenTower = new ImageIcon("textures" + File.separator + "torony1G_button.png");
	    JButton greenTowerBtn = new JButton(greenTower);
	    greenTowerBtn.setBorderPainted(false);
	    greenTowerBtn.setName("greenTower");
	    greenTowerBtn.setFocusable(false);
	    greenTowerBtn.setOpaque(false);
	    greenTowerBtn.setContentAreaFilled(false);
	    greenTowerBtn.addActionListener(this);
	    greenTowerBtn.setPreferredSize(new Dimension(100, 120));
	    buttonList.add(greenTowerBtn);
	    
	    ImageIcon codefreeze = new ImageIcon("textures" + File.separator + "codefreeze.png");
	    JButton startBtn = new JButton(codefreeze);
	    startBtn.setPreferredSize(new Dimension(130, 130));
	    startBtn.setOpaque(false);
	    startBtn.setContentAreaFilled(false);
	    startBtn.setBorderPainted(false);
//	    buttonList.add(startBtn);
	    
	    startBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				((JButton)(e.getSource())).setEnabled(false);
				controller.startGame();
			}
		});
	    
	    int mana = controller.getSaruman().getMana();
	    if(mana < 0) mana = 0;
	    sarumanLabel = new JLabel(" " + mana + " ", SwingConstants.CENTER);
	    sarumanLabel.setFont(sarumanLabel.getFont().deriveFont(26.0f));
	    sarumanLabel.setOpaque(false);
	    sarumanLabel.setPreferredSize(new Dimension(120, 130));
	    
	    sarumanPanel = new DrawRectPanel();
	    sarumanPanel.setOpaque(false);
	    sarumanPanel.setPreferredSize(new Dimension(900, 130));
	    
	    
	    controllPanel.add(menuBtn);
	    controllPanel.add(startBtn);
	    controllPanel.add(sarumanLabel);
	    controllPanel.add(sarumanPanel);
	    controllPanel.add(greenTowerBtn);
	    controllPanel.add(blackTowerBtn);
	    controllPanel.add(blueTowerBtn);
	    controllPanel.add(yellowTowerBtn);
	    controllPanel.add(redTowerBtn);
	    
	    gameFrame.remove(menuPanel);
	    mapPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
	    gameFrame.add(controllPanel, BorderLayout.SOUTH);
	    gameFrame.add(mapPanel, BorderLayout.NORTH);
	    gameFrame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(selectedButton!=null)
		{
			selectedButton.setBackground(null);
		}
		
    	selectedButton=(JButton) e.getSource();
    	selectedButton.setOpaque(true);
    	selectedButton.setContentAreaFilled(true);
    	for(JButton jb : buttonList) {
    		if (jb != selectedButton) {
    	    	jb.setOpaque(false);
    	    	jb.setContentAreaFilled(false);
    		}
    	}
	}

	public void setSarumanLabelText(String str) {
		sarumanLabel.setText(str);
		
	}
	
	public void setTowerOpague(String str) {
		sarumanLabel.setText(str);
		
	}

	public void setDeliverySign(boolean b) {
		mapView.setDeliverySign(b);
	}
}
