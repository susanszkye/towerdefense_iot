package hu.xpathfactory.towerdefense.view;

import hu.xpathfactory.towerdefense.model.Saruman;

public class SarumanView {

	private Saruman saruman;
	
	public SarumanView(Saruman s) {
		saruman = s;
	}
	
	public void addGameView(GameView gameView) {
		saruman.addGameView(gameView);
		
	}

}
