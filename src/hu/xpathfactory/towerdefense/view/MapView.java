package hu.xpathfactory.towerdefense.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import hu.xpathfactory.towerdefense.model.Map;
import hu.xpathfactory.towerdefense.model.Road;
import hu.xpathfactory.towerdefense.model.Tower;
import hu.xpathfactory.towerdefense.util.Coordinate;

public class MapView {

	private Map map;
	private ArrayList<TowerView> towerviews;
	private ArrayList<RoadView> roadviews;
	private ArrayList<ProgrammerView> programmerViews;
	private BufferedImage lorry;
	private BufferedImage background;
	private boolean deliverySign;
	private String deliveryText;
	private ImageReader imageReader;
	
	public MapView(Map m, ImageReader imageReader) {
		this.imageReader = imageReader;
		deliveryText = null;
		map = m;
		programmerViews = new ArrayList<ProgrammerView>();
	//	lorry = imageReader.readImage("textures/lorry.png");
		background = imageReader.readImage("textures/proba_city5.png");
		
		towerviews = new ArrayList<TowerView>();
		List<Tower> towers = map.getTowers();
		for (int i = 0; i < towers.size(); i++ ) {
			towerviews.add(new TowerView(towers.get(i), imageReader));
		}
		
		roadviews = new ArrayList<RoadView>();
		List<Road> roads = map.getRoads();
		for (int i = 0; i < roads.size(); i++ ) {
			Road road = roads.get(i);
			roadviews.add(new RoadView(road, imageReader));
		}

		Coordinate c = new Coordinate(110, 567);
		ProgrammerView p = new ProgrammerView(c, 90, imageReader);
		programmerViews.add(p);

		c = new Coordinate(325, 85);
		p = new ProgrammerView(c, 180, imageReader);
		programmerViews.add(p);

		c = new Coordinate(165, 400);
		p = new ProgrammerView(c, 23, imageReader);
		programmerViews.add(p);
		
		c = new Coordinate(500, 920);
		p = new ProgrammerView(c, 0, imageReader);
		programmerViews.add(p);
		
		c = new Coordinate(720, 170);
		p = new ProgrammerView(c, 135, imageReader);
		programmerViews.add(p);
	}
	
	public void draw(Graphics g) {

		g.drawImage(background, 0, 0, null);
		
		List<Tower> towers = map.getTowers();
		if (towerviews.size() < towers.size()) {
			for (int i = towerviews.size(); i < towers.size(); i++ ) {
				towerviews.add(new TowerView(towers.get(i), imageReader));
			}
		}
		
		List<Road> roads = map.getRoads();
		if (roadviews.size() < roads.size()) {
			for (int i = roadviews.size(); i < roads.size(); i++ ) {
				roadviews.add(new RoadView(roads.get(i), imageReader));
			}
		}
		
		for (int i = 0; i < roadviews.size(); i++ ) {
			roadviews.get(i).draw(g);
		}
		for (int i = 0; i < roadviews.size(); i++ ) {
			roadviews.get(i).drawTrapsAndEnemies(g); //azert kellett, mert ha keresztezodesben fedik
													//egymast az ut darabok akkor felulirja
		}
		for (int i = 0; i < towerviews.size(); i++ ) {
			towerviews.get(i).draw(g, map.getMoney());
		}
		for (ProgrammerView p : programmerViews) {
			p.draw(g);
		}
		
	//	g.drawImage(lorry, 1470 - lorry.getWidth()/2, 750 - lorry.getHeight()/2, null);
		if (deliverySign) {
//			g.setColor(Color.RED);
//			g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
		//	g.drawString(deliveryText, 1415, 750);
		} else {
//			g.setColor(Color.BLACK);
//			g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 26));
			//g.drawString(map.getGoodEnemyCount() + " / 5", 1455, 750);
		}
	}

	public void setDeliverySign(boolean b) {
		deliverySign = true;
//		if(b) {
//			
//			deliveryText = ":(  :(  :(  :(  :(";
//		} else {
//			deliveryText = ":)  :)  WIN  :)  :)";
//		}
	}
	
}
