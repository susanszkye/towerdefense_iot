package hu.xpathfactory.towerdefense.model;

import hu.xpathfactory.towerdefense.controller.Controller;

import java.util.List;
import java.util.Random;

public class Cross {
	private int distance;
	private List<Road> roads;
	
	/**
	 * Keresztezodes konstruktora
	 * @param distance
	 * 					A keresztezodes tavolsaga a vegzet hegyetol
	 * @param roads
	 * 					A keresztezodes utjai
	 */
	public Cross(int distance, List<Road> roads) {
		this.setDistance(distance);
		this.setRoads(roads);
	}
	
	
	/**
	 * Visszaadja a distance parametert
	 * @return
	 * 			A distance erteke
	 */
	public int getDistance() {
		return distance;
	}
	
	/**
	 * Visszaadja a kovetkezo utat
	 * @return
	 * 			Kovetkezo ut
	 */
	public Road getNextRoad() {
		if (Controller.NEXT_ROAD != -1) {
			if(Controller.NEXT_ROAD > roads.size() - 1) {
				return null;
			}
			return roads.get(Controller.NEXT_ROAD);
		} else {
			return roads.get(new Random().nextInt(roads.size()));
		}
	}
	
	/**
	 * Beallitja a distance-t
	 * @param distance
	 * 					A beallitando distance mennyisege
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	/**
	 * Visszaadja, hogy a keresztezodes milyen utakba kapcsolodik
	 * @return
	 * 			Az utak listaja
	 */
	public List<Road> getRoads() {
		return roads;
	}
	
	/**
	 * Beallitja az ut listat
	 * @param roads
	 * 				Beallitando utlista
	 */
	public void setRoads(List<Road> roads) {
		this.roads = roads;
	}
	

}
