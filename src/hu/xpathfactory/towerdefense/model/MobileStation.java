package hu.xpathfactory.towerdefense.model;


import hu.xpathfactory.towerdefense.util.Coordinate;
import hu.xpathfactory.towerdefense.util.NotifyNextTurn;


public abstract class MobileStation implements NotifyNextTurn {
	public static final int PRICE = 5;
	
	private final int turnsToStep;
	private int turnsSinceLastStep;
	protected double dataIn;
	protected int dataToHappiness;
	protected int stepsCount;
	protected Road road;
	private EndUserType race;
	private boolean gotData;
	/**
	 * Konstruktor
	 * @param turnsToStep
	 * 						mily
	 * en surun lepjen
	 * @param turnsToStepActual
	 * 						mennyi van meg hatra a kovetkezo lepesig
	 * @param turnsSinceLastStep
	 * 						mennyi telt el az elozo lepes ota
	 * @param dataIn
	 * 						fogadott data
	 * @param dataIn
	 * 						data, amit el kell erni, hogy boldog legyen
	 * @param stepsCount
	 * 						hany lepes telt el azota
	 * @param road
	 * 						melyik uton megy
	 * @param race
	 * 						a faja
	 */
	public MobileStation(int turnsToStep, int turnsToStepActual, int dataToHappiness, Road road,
			EndUserType race/*, boolean isGood*/) {
		super();
		this.turnsToStep = turnsToStep * 10;
		this.turnsSinceLastStep = 0;
		this.dataIn = 1;
		this.dataToHappiness = dataToHappiness;
		this.stepsCount = 1;
		this.road = road;
		this.race = race;
		this.gotData = false;
	//	this.isGood = isGood;
		
	}
	/**
	 * 
	 * @param data -Kapott data
	 */
	public void reveiveData(int data) {
		dataIn += data;
		if(dataIn > dataToHappiness) {
			dataIn = dataToHappiness;
		}
		gotData = data > 0;
	}
	
//	public boolean isGood(){
//		return isGood;
//	}
	
	/**
	 * Lek�rdezi az ellenf�l faj�t, �s visszat�r vele
	 * @return a p�ld�ny faja
	 */	
	public EndUserType getRace() {	
		return race;
	}
	
	public Road getRoad(){
		return road;
	}
	
	public double getData(){
		return dataIn;
	}
	
	public int getDataToHappiness(){
		return dataToHappiness;
	}
	
	/**
	 * Visszadja az edig megtett l�p�seket
	 * @return
	 */
	public int getNumberOfSteps() {
		return this.stepsCount;
	}
	
	public Coordinate getCoordinate() {
		return road.getPoints().get(stepsCount);
	}
	
	/**
	 * Ellenf�l megkezdi az �j k�rt
	 */
	@Override
	public boolean nextTurnStarted() {
		if (turnsSinceLastStep >= turnsToStep /*turnsToStepActual*/) {
			turnsSinceLastStep = 0;
			if (road.hasNextPoint(stepsCount)) {
			 //   turnsToStepActual = turnsToStep;
				this.stepsCount++;
			} else if (road.hasCross()) {
				this.road.addDeletedEnemy(this);
				road.getNextRoad().addEnemy(this); // itt allitjuk at az utat a kovetkezore... es le is nullazzuk a stepcountot.........
				this.stepsCount++; // nem kene itt lenullazni?? lsd egy sorral feljebb: nem
			} else {
			/*	if(isGood) {
					road.incrementGoodEnemyCount();
					this.road.addDeletedEnemy(this);
				} else {*/
					road.endGame(true);
//				}
			}  
			dataIn -= 0.001*dataToHappiness;
			if (dataIn < 0) {
				dataIn = 0;
			}
			return true;
		}
		turnsSinceLastStep ++;
		return false;
	}
	

	/**
	 * Ellenf�l be�ll�tja az utat, amelyikre helyezt�k
	 * @param r
	 */
	public void setRoad(Road r) {
		road = r;
		stepsCount = 0;
	}
	
	/**
	 * Beallitja az eleterot
	 * @param health
	 * 					Beallitando mennyiseg
	 */
	public void setHealth(int health) {
		this.dataIn = health;
	}
	
	/**
	 * Beallitja a lepesszamlalot
	 * @param stepsCount
	 * 					Beallitando mennyiseg
	 */
	public void setStepsCount(int stepsCount) {
		this.stepsCount = stepsCount;
	}
	public int getDirection() {
		return road.getDirection();
	}
	public void setGotDataFalse() {
		gotData = false;
	}
	public boolean hasReceiveData() {
		return gotData;
	}
	public int getHappinessLevel() {
		return (int) ((((double)dataIn/(double)dataToHappiness) * 10) - 5);
	}
	public double roadDirection() {
		return road.getDirection();
	}
}
