package hu.xpathfactory.towerdefense.model;

import hu.xpathfactory.towerdefense.controller.Controller;
import hu.xpathfactory.towerdefense.util.Coordinate;
import hu.xpathfactory.towerdefense.util.NotifyNextTurn;
import hu.xpathfactory.towerdefense.util.Runeable;
import hu.xpathfactory.towerdefense.view.MapView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Map implements NotifyNextTurn {
	private static final int turnsToShoot = 400;
	private List<Road> roads;
	private List<Tower> towers;
	private Controller controller;
	private int turnsSinceLastShoot;
	
	/**
	 * Map konstruktora.
	 * 
	 * @param controller
	 *            a maphez tartoz� kontroller
	 */
	public Map(Controller controller) {
		roads = new LinkedList<Road>();
		towers = new ArrayList<Tower>();
		this.controller = controller;
		turnsSinceLastShoot = 0;
	}
	
	public void incrementGoodEnemyCount() {
		controller.setGoodEnemyCount();
	}
	
	/**
	 * Ut hozzaadasa a maphaz
	 * @param points
	 * 				Ut pontjainak listaja
	 */
	public void addRoad(ArrayList<Coordinate> points){
			roads.add(new Road(this, points));
	}

	/**
	 * 
	 * @param c
	 *            -A torony koordin�t�ja.
	 * @param range
	 *            -A torony hat�sugara.
	 * @return A hegyhez legk�zelebb l�v� ellenf�l.
	 * @throws IOException
	 */
	public ArrayList<MobileStation> getMobileUsersInRange(Coordinate c, double range, Color color) {
		ArrayList<MobileStation> mobileStations = new ArrayList<MobileStation>();
					
		for(Road road : roads) {
			mobileStations.addAll(road.getMobileUsersInRange(c, range, color));
		}
		return mobileStations;

	}
	
	public void setGotDataFalse() {
		for (Road road : roads) {
			road.setGotDataFalse();
		}
	}
	
	public void setDirection(int roadNumber, int direction) {
		roads.get(roadNumber).setDirection(direction);
	}

	/**
	 * Visszaadja az utak szamat
	 * 
	 * @return
	 */
	public int getNumberOfRoad() {
		return roads.size();
	}

	/**
	 * Ellenf�l �tad�sa a megfelel� �tnak
	 * 
	 * @param road
	 *            az �t sz�ma
	 * @param e
	 *            az �zhoz adand� ellenf�l
	 * @return true, ha sikeres, false ha nem
	 */
	public boolean addEnemyToRoad(int road, MobileStation e) {
		roads.get(road).addEnemy(e);
		return true;
	}

	/**
	 * Az adott torony lovesenek nagysagat adja vissza.
	 * @param c a torony koordinataja.
	 * @return loves nagysaga
	 */
	public int getTowerFireRate(Coordinate c) {
		for(Tower t : towers) {
			if (t.getCoordinate() == c) {
				return t.getDamage();
			}
		}
		return 0;
	}

	/**
	 * Az adott torony lovesi tavolsagat adja vissza.
	 * @param c a torony koordinataja.
	 * @return a loves tavolsaga
	 */
	public int getTowerFireRange(Coordinate c) {
		for(Tower t : towers) {
			if (t.getCoordinate() == c) {
				return t.getFireRange();
			}
		}
		return 0;
	}

	/**
	 * Road h�vja, ha meghalt az egyikk ellenfele
	 */
	public void enemyDied() {
		controller.enemyDied();
	}

	/**
	 * �j tornyot rak le a t�rk�pre
	 * 
	 * @param t
	 */
	public void addTower(Tower t) {
		towers.add(t);
	}


	/**
	 * Adott koordinata az utnak pontja e
	 * @param c
	 * 			Adott koodinata
	 * @return
	 * 			Igen ha az adott koordinata egy ut pontja
	 */
	public boolean isItRoad(Coordinate c, double r) {
		for (Road road : roads) {
			List<Coordinate> points = road.getPoints();
			for (Coordinate coord : points) {
				int dx = 0;
				int dy = 0;
				
				if (coord.getX() > c.getX()) {
					dx = coord.getX() - c.getX();
				}
				if (coord.getX() < c.getX()) {
					dx = c.getX() - coord.getX();
				}
				if (coord.getY() > c.getY()) {
					dy = coord.getY() - c.getY();
				}
				if (coord.getY() < c.getY()) {
					dy = c.getY() - coord.getY();
				}
				
				double distance;
				if (dx == 0) {
					distance = dy;
				} else if (dy == 0) {
					distance = dx;
				} else {
					distance = Math.sqrt((dx * dx) + (dy * dy));
				}

				if ((distance - r) < 0) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Megmutatja, hogy az adott helyen van-e m�r �t vagy torony.
	 * 
	 * @param c
	 *            a koordin�ta, ahol megn�zi, hogy van-e valami
	 * @param r
	 *            ekkora sugar� k�rben
	 * @return hamis, ha nincs, igaz, ha van
	 */
	public boolean isThereAnything(Coordinate c, double r) {
		if (isItRoad(c, r))
			return true;
		for (Tower tower : towers) {
			
			int dx = 0;
			int dy = 0;
			
			if (tower.getX() > c.getX()) {
				dx = tower.getX() - c.getX();
			}
			if (tower.getX() < c.getX()) {
				dx = c.getX() - tower.getX();
			}
			if (tower.getY() > c.getY()) {
				dy = tower.getY() - c.getY();
			}
			if (tower.getY() < c.getY()) {
				dy = c.getY() - tower.getY();
			}
			
			double distance;
			if (dx == 0) {
				distance = dy;
			} else if (dy == 0) {
				distance = dx;
			} else {
				distance = Math.sqrt((dx * dx) + (dy * dy));
			}

			if ((distance - r) < 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Visszaadja azt a csapd�t vagy tornyot, ahova helyezni lehet a k�vet.
	 * 
	 * @param c
	 *            a koordin�ta, ahol csapd�t vagy tornyot keres.
	 * @return csapda vagy torony
	 */
	public Runeable whatIsThere(Coordinate c) {
		for (Tower tower : towers) {

			int dx = 0;
			int dy = 0;
			
			if (tower.getX() > c.getX()) {
				dx = tower.getX() - c.getX();
			}
			
			if (tower.getX() < c.getX()) {
				dx = c.getX() - tower.getX();
			}
			
			if (tower.getY() > c.getY()) {
				dy = tower.getY() - c.getY();
			}
			
			if (tower.getY() < c.getY()) {
				dy = c.getY() - tower.getY();
			}
			
			double distance;
			if(dx == 0 && dy == 0){
				distance = 0;
			} else if (dx == 0) {
				distance = dy;
			} else if (dy == 0) {
				distance = dx;
			} else {
				distance = Math.sqrt((dx * dx) + (dy * dy));
			}

			if ((distance - 30) < 0) {
				return tower;
			}
		}

//		for (Road road : roads) {
//			Runeable runeable = road.whatIsThere(c);
//			if (runeable != null)
//				return runeable;
//		}

		return null;
	}

	/**
	 * Map megkezdi az �j k�rt
	 */
	@Override
	public boolean nextTurnStarted() throws NumberFormatException {
		
		for (Road road : roads) {
			road.nextTurnStarted();
		}
		
		if (turnsSinceLastShoot == turnsToShoot) {
			this.setGotDataFalse();
			turnsSinceLastShoot = 0;
			for (Tower tower : towers) {
				controller.addSarumanMana(-tower.maintenanceCost());
				tower.nextTurnStarted();
			}
			for (Road road : roads) {
				controller.addSarumanMana(road.getHappinessLevel());
			}
		} else {
			turnsSinceLastShoot++;
		}


		return false;
	}
	
	/**
	 * �rtes�ti a kontrollert, hogy v�ge a j�t�knak
	 */
	public void endGame(boolean b) {
		controller.endGame(b);
	}

	/**
	 * Visszaadja az utak listaja
	 * @return
	 * 			Az utak listaja
	 */
	public List<Road> getRoads() {
		return roads;
	}

	/**
	 * Visszaadja a tornyok listajat
	 * @return
	 * 			A tornyok listaja
	 */
	public List<Tower> getTowers() {
		return towers;
	}
	
	public boolean hasEnemy(){
		boolean ret = false;
		for(Road road : roads) {
			ret |= road.hasEnemy();
		}
		return ret;
	}

	public int getGoodEnemyCount() {
		return controller.getGoodEnemyCount();
	}

	public void gameStarted() {
		for(Tower t: towers) {
			t.setGameStarted();
		}
	}
	
	public int nrOfTowers() {
		return towers.size();
	}

	public int getMoney() {
		return controller.getSaruman().getMana();
	}

	public Controller getController() {
		return this.controller;
	}

	public MapView mapView() {
		// TODO Auto-generated method stub
		return mapView();
	}
}
