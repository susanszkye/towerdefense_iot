package hu.xpathfactory.towerdefense.model;


public class TowerFiveG extends Tower {

	public static final int PRICE = 1000;
	/**
	 * konstruktor
	 * @param map
	 */
	public TowerFiveG(Map map) {
		super(map);
		data = CAPACITY_5G;
		turnsToShoot = SHOOTRATE_MID;
		range = RANGE_MID;
		color = Color.Red;
		price = PRICE;
		maintenanceCost = 10;
		nrOfMSsToGetData = 10;
	}
	@Override
	public String getGeneration() {
		return "5";
	}
}
