package hu.xpathfactory.towerdefense.model;

public enum EndUserType {
	SimpleCall, MobileNet, VideoStream, Car
}
