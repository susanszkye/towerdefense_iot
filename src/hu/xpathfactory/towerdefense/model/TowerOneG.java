package hu.xpathfactory.towerdefense.model;

public class TowerOneG extends Tower {
	public static final int PRICE = 25;
	
	public TowerOneG(Map map) {
		super(map);
		data = CAPACITY_1G;
		turnsToShoot = SHOOTRATE_MID;
		range = RANGE_LOW;
		color = Color.Green;
		price = PRICE;
		maintenanceCost = 1;
		nrOfMSsToGetData = 2;
	}

	@Override
	public String getGeneration() {
		return "1";
	}
}
