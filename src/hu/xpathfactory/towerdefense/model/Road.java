package hu.xpathfactory.towerdefense.model;

import hu.xpathfactory.towerdefense.util.Coordinate;
import hu.xpathfactory.towerdefense.util.NotifyNextTurn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Road implements NotifyNextTurn {
	private List<Coordinate> points;
	private List<MobileStation> enemies;
	private int length;
	private Map map;
	private Cross cross;
	private List<MobileStation> deletedEnemy;
	private int direction;

	/**
 	* Konstruktor 
 	* @param map
 	* 			A terkep amin elhelyezkedik
 	* @param points
 	* 			Az ut pontjai
 	*/
	public Road(Map map, List<Coordinate> points) {
		this.enemies = new ArrayList<MobileStation>();
		this.points = new ArrayList<Coordinate>();
		this.points.addAll(points);
		this.length = points.size();
		this.map = map;
		deletedEnemy=new ArrayList<MobileStation>();
		direction = 0;
	}
	
	public int getRoadLenght() {
		return points.size();
	}
	
	public Coordinate getCoordinate(int i) {
		return points.get(i);
	}
	
	/**
	 * Visszaadja a keresztezodest
	 * @return
	 * 			A keresztezodes
	 */
	public Cross getCross(){
		return cross;
	}
	
	/**
	 * Meg�llap�tja, hogy az ellenf�lre mekkora lass�t�s hat a ponton ahol van.
	 * 
	 * @param e
	 *            - a lass�tand� ellenf�l
	 * @return a lass�t�s �rt�ke
	 * @throws IOException
	 */
	public int getAmountOfSlow(MobileStation e) {
		int amountOfSlow = 0;

		return amountOfSlow;
	}

	/**
	 * Megadja, hogy van-e m�g k�vetkez� pontja az �tnak.
	 * 
	 * @param steps
	 *            - az �ton megtett l�p�sek sz�ma
	 * @return igaz, ha van, hamis, ha nincs
	 */
	public boolean hasNextPoint(int steps) {
		boolean hasNext = (length-1 > steps);
		return hasNext;
	}

	/**
	 * 
	 * @param p
	 *            -A torony koordin�t�ja
	 * @param range
	 *            -A torony hat�sugara.
	 * @return -A hegyhez legk�zelebb l�v� ellenf�l t�vols�ga ezen az �ton.
	 * @throws IOException
	 */
/*	public int getClosestEnemyDistanceInRange(Coordinate towerLocation, double towerRange, Color color) {

		MobileStation closestEnemy = null;
		EndUserType race = EndUserType.Car;
		switch (color) {
		case Red: race = EndUserType.Car; break;
		case Blue: race = EndUserType.MobileNet; break;
		case Yellow: race = EndUserType.VideoStream; break;
		default:
			break;
		}
		
		for (MobileStation enemy : enemies) {
			if ( !enemy.isGood() && (enemy.getRace() == race) || color == Color.Black) {
				Coordinate enemyLocation = points.get(enemy.getNumberOfSteps());
				int x = enemyLocation.getX() - towerLocation.getX();
				int y = enemyLocation.getY() - towerLocation.getY();
				double distance = Math.sqrt((x * x) + (y * y));
				if (distance < towerRange) {
					if (closestEnemy == null) {
						closestEnemy = enemy;
					} else {
						if (closestEnemy.getNumberOfSteps() < enemy.getNumberOfSteps()) {
							closestEnemy = enemy;
						}
					}
				}
			}
		}

		if (closestEnemy == null) {
			return -1;
		} else {
			return length - closestEnemy.getNumberOfSteps() + (hasCross()? cross.getDistance() : 0);
		}
	}*/

	public void setGotDataFalse() {
		for (MobileStation enemy : enemies) {
			enemy.setGotDataFalse();
		}
	}
	
	/**
	 * 
	 * @param c
	 *            -A torony koordin�t�ja.
	 * @param range
	 *            -A torony hat�sugara.
	 * @return A hegyhez legk�zelebb l�v� ellenf�l.
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public ArrayList<MobileStation> getMobileUsersInRange(Coordinate towerLocation, double towerRange, Color color) throws NumberFormatException {

		ArrayList<MobileStation> mobileStationsInRange = new ArrayList<MobileStation>();
		EndUserType race = EndUserType.Car;
		switch (color) {
		case Green: race = EndUserType.Car; break;
		case Yellow: race = EndUserType.MobileNet; break;
		case Red: race = EndUserType.VideoStream; break;
		case Blue: race = EndUserType.SimpleCall; break;
		default:
			break;
		}
		
		for (MobileStation mobileStation : enemies) {
		//	if( /*!enemy.isGood() &&*/ (mobileStation.getRace() == race) ) {
				Coordinate enemyLocation = points.get(mobileStation.getNumberOfSteps());
				int x = enemyLocation.getX() - towerLocation.getX();
				int y = enemyLocation.getY() - towerLocation.getY();
				double distance = Math.sqrt((x * x) + (y * y));
				if (distance < towerRange) {
					mobileStationsInRange.add(mobileStation);
				}
		//	}
		}
		return mobileStationsInRange;
	}

	/**
	 * 
	 * @param e
	 *            T�r�lni k�v�nt ellens�g.
	 * 
	 */
	public void deleteEnemy(MobileStation e) {
		deleteEnemy(e,true);
	}
	
	public void deleteEnemy(MobileStation e, Boolean isDead) {
		enemies.remove(e);
		if (isDead){
			map.enemyDied();
		}
	}

	/**
	 * Hozz�adja a kapott ellenfelet az �thoz.
	 * 
	 * @param e
	 *            ellenf�l
	 */
	public void addEnemy(MobileStation e) {
		e.setRoad(this);
		enemies.add(e);
	}


	/**
	 * Eld�nti, hogy a kapott pont az �t r�sze-e
	 * 
	 * @param c
	 *            Coordin�ta
	 * @return true, ha az �t r�sze, false, ha nem
	 */
	public int getPositionOnRoad(Coordinate c) {
		double min = 21;
		int ret = -1;
		for (int i = 0; i < points.size(); i++) {
			int x = points.get(i).getX() - c.getX();
			int y = points.get(i).getY() - c.getY();
			double distance = Math.sqrt((x * x) + (y * y));
			if (distance < 20) {
				if(distance < min){
					min = distance;
					ret = i;
				}
			}
		}
		return ret;
	}

	/**
	 * �t megkezdi az �j k�rt
	 */
	@Override
	public boolean nextTurnStarted() throws NumberFormatException {
		deletedEnemy.clear();
		for (MobileStation enemy : enemies) {
			enemy.nextTurnStarted();
		}
		for (MobileStation e : deletedEnemy){
			enemies.remove(e);
		}
		return true;
	}

	/**
	 * Visszaad egy csapd�t, ahova a k�vet le lehet rakni.
	 * 
	 * @param c
	 *            a koordin�ta, ahol keres
	 * @return csapda, ha tal�lt, null, ha nem
	 */
//	public Trap whatIsThere(Coordinate c) {
//
//		return null;
//	}

	/**
	 * Van e az utnak keresztezodese
	 * @return
	 * 			igen, ha az utnak van keresztezodese
	 */
	public boolean hasCross() {
		return cross == null ? false : true;
	}

	/**
	 * Visszaaja a kovetkezo utat ha van
	 * @return
	 * 			A kovetkezo ut ha van
	 */
	public Road getNextRoad() {
		if (hasCross()) {
			return cross.getNextRoad();
		}
		return null;
	}

	/**
	 * beallitja a jatek veget
	 * @param winning
	 * 					jatek vege
	 */
	public void endGame(Boolean winning) {
		map.endGame(winning);
	}

	/**
	 * Az ut pontjainak koordianatajat adja vissza
	 * @return
	 * 			Az ut pontjainak koordinatai
	 */
	public List<Coordinate> getPoints() {
		return points;
	}

	/**
	 * Uton levo ellenfelek listaja
	 * @return
	 * 			Az ellenfelek listaja
	 */
	public List<MobileStation> getEnemies() {
		return enemies;
	}

	public void setCross(Cross cross) {
		this.cross = cross;
	}
	
	public void addDeletedEnemy(MobileStation e){
		deletedEnemy.add(e);
	}
	
	public boolean hasEnemy(){
		return enemies.size() != 0;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public void incrementGoodEnemyCount() {
		map.incrementGoodEnemyCount();
	}

	public int getHappinessLevel() {
		int happyness = 0;
		for(MobileStation ms : enemies) {
			happyness += ms.getHappinessLevel();
		}
		return happyness;
	}

}
