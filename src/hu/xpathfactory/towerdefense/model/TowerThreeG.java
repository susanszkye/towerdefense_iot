package hu.xpathfactory.towerdefense.model;


public class TowerThreeG extends Tower{

	public static final int PRICE = 100;
	
	/**
	 * konstruktor
	 * @param map
	 */
	public TowerThreeG(Map map) {
		super(map);
		data = CAPACITY_3G;
		turnsToShoot = SHOOTRATE_MID;
		range = RANGE_MID;
		color = Color.Blue;
		price = PRICE;
		maintenanceCost = 5;
		nrOfMSsToGetData = 5;
	}

	@Override
	public String getGeneration() {
		return "3";
	}

}
