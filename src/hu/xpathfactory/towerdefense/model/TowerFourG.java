package hu.xpathfactory.towerdefense.model;


public class TowerFourG extends Tower{

	public static final int PRICE = 500;
	
	/**
	 * konstruktor
	 * @param map
	 */
	public TowerFourG(Map map) {
		super(map);
		data = CAPACITY_4G;
		turnsToShoot = SHOOTRATE_MID;
		range = RANGE_MID;
		color = Color.Yellow;
		price = PRICE;
		maintenanceCost = 7;
		nrOfMSsToGetData = 7;
	}

	@Override
	public String getGeneration() {
		return "4";
	}

}