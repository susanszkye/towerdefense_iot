package hu.xpathfactory.towerdefense.model;

import java.util.ArrayList;

import hu.xpathfactory.towerdefense.util.Coordinate;
import hu.xpathfactory.towerdefense.util.NotifyNextTurn;
import hu.xpathfactory.towerdefense.util.Runeable;

public abstract class Tower implements Runeable, NotifyNextTurn {

	protected static final int CAPACITY_1G = 7;
	protected static final int CAPACITY_2G = 9;
	protected static final int CAPACITY_3G = 12;
	protected static final int CAPACITY_4G = 50;
	protected static final int CAPACITY_5G = 100;
	protected static final int SHOOTRATE_LOW = 800;
	protected static final int SHOOTRATE_MID = 400;
	protected static final int SHOOTRATE_HIGH = 200;
	protected static final int RANGE_LOW = 180;//90;
	protected static final int RANGE_MID = 210; //105;
	protected static final int RANGE_HIGH = 240; //120;
	public static final int PRICE = 25;

	protected int turnsToShoot;
	private Coordinate position;
	protected int data;
	protected int range;
	private Map map;
	private ArrayList<MobileStation> shootedEnemies = new ArrayList<MobileStation>();
	protected Color color;
	protected int price;
	protected int maintenanceCost;
	private boolean gameStarted;
	protected int nrOfMSsToGetData;
	
	/**
	 * Konstruktor
	 * 
	 * @param map
	 */
	public Tower(Map map) {
		this.map = map;
		gameStarted = false;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}
		
	public Color getColor() {
		return color;
	}
	
	/**
	 * Megkezdi az �j k�rt
	 */
	@Override
	public boolean nextTurnStarted() {
		int rangetoMap = range;
		ArrayList<MobileStation> mobileStationsInRange = map.getMobileUsersInRange(position, rangetoMap, color);
		
		// Ha van kit loni
		if (mobileStationsInRange.size() > 0) {
//			int nrOfMSsToGetData = 5;
			int received = 0;
			for(MobileStation ms : mobileStationsInRange) {
				if(!ms.hasReceiveData() && received < nrOfMSsToGetData) {
					ms.reveiveData(data);
					shootedEnemies.add(ms);
					received++;
				}
			}
		}
		return true;
	}


	public Coordinate whereToShoot() {		
		Coordinate c = shootedEnemies.get(0).getCoordinate();
		shootedEnemies.remove(0);
		return c;
	}
	public boolean isShomethingToShoot() {
		return (shootedEnemies.size() > 0) ? true : false;
	}
	/**
	 * Visszaadja a torony sebzeset
	 * @return
	 * 		A torony sebzese
	 */
	public int getDamage() {
		return data;
	}

	/**
	 * Visszaadja a torony tuzelesi tavolsagat
	 * @return
	 * 			A torony tuzelesi tavolsaga
	 */
	public int getFireRange() {
		return range;
	}

	/**
	 * Visszadaja a pozici�j�t
	 */
	public Coordinate getCoordinate() {
		return position;
	}
	
	public int getX() {
		return position.getX();
	}
	
	public int getY() {
		return position.getY();
	}

	public void setGameStarted() {
		gameStarted = true;
	}

	public boolean hasGameStarted() {
		return gameStarted;
	}

	public int getPrice() {
		// TODO Auto-generated method stub
		return price;
	}

	public int maintenanceCost() {
		return maintenanceCost;
	}

	public abstract String getGeneration();

}
