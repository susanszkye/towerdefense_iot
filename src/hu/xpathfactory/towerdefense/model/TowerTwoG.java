package hu.xpathfactory.towerdefense.model;


public class TowerTwoG extends Tower{

	public static final int PRICE = 50;
	
	/**
	 * konstruktor
	 * @param map
	 */
	public TowerTwoG(Map map) {
		super(map);
		data = CAPACITY_2G;
		turnsToShoot = SHOOTRATE_MID;
		range = RANGE_MID;
		color = Color.Black;
		price = PRICE;
		maintenanceCost = 3;
		nrOfMSsToGetData = 3;
	}

	@Override
	public String getGeneration() {
		return "2";
	}
}