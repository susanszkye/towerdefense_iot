package hu.xpathfactory.towerdefense.model;
import hu.xpathfactory.towerdefense.view.GameView;


public class Saruman {
	private static final int START_MANA =1000;
	
	private int mana;
	private boolean isDead;

	private GameView gameView;
	
	/**
	 * Saruman konstruktora.
	 *  @param diff jatek nehezsege, amibol kiszamolja a kezdeti manat
	 *  @param controler jatek controlerje
	 */
	public Saruman() {
		mana = START_MANA;
		isDead = false;
		
	}
	
	/**
	 * Hozz�adja a kapott man�t a meglev�h�z
	 * @param mana
	 */
	public void addMana(int mana) {
	//	if (this.mana + mana < START_MANA) {
			this.mana += mana;
	//	} 
//		else {
//			this.mana = START_MANA;
//		}
		if(this.mana > 0) gameView.setSarumanLabelText(" " + this.mana + " ");
		else gameView.setSarumanLabelText(":(   :(   :(   :(   :(");
	}
	
	/**
	 * Megadja, hogy Saruman �l e
	 * @return
	 */
	public boolean isItDead(){
		return isDead;
	}
	
	/**
	 * Be�ll�tja szarum�nt halottnak
	 * @param b
	 */
	public void setDeath(boolean b){
		this.isDead = b;
	}
	
	/**
	 * Megadja, hogy van-e el�g man�ja Szarum�nnak.
	 * @param cost a lerakni k�v�nt dolog �ra
	 * @return igaz, ha van, hamis, ha nincs
	 */
	public boolean hasEnoughMana(int cost){
		if(this.mana >= cost){
			return true;
		}	
		return false;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public void addGameView(GameView gameView) {
		this.gameView = gameView;
	}

	public int startMana() {
		return START_MANA;
	}
}
