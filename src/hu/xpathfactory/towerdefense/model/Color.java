package hu.xpathfactory.towerdefense.model;

public enum Color {
	Black, Blue, Green, Red, Yellow
}
